/*Confif */
// Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";
  
import { getDatabase,onValue,ref,set,get,child,update,remove } from
"https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL} from
"https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyAdSdj6Et-MvxTHQQtM8Ac2W6jWAVSprP4",
    authDomain: "fireserrano-832f7.firebaseapp.com",
    projectId: "fireserrano-832f7",
    databaseURL:"https://fireserrano-832f7-default-rtdb.firebaseio.com/",
    storageBucket: "fireserrano-832f7.appspot.com",
    messagingSenderId: "160793656459",
    appId: "1:160793656459:web:11308caac1ee2f7b27288e",
    measurementId: "G-RQN69192B5"
  };

  // Initialize Firebase
    const app= initializeApp(firebaseConfig);
    const db = getDatabase();
    

    var btnInsertar = document.getElementById("btnInsertar");
    var btnBuscar = document.getElementById("btnBuscar");
    var btnActualizar = document.getElementById("btnActualizar");
    var btnBorrar = document.getElementById("btnBorrar");
    var btnTodos = document.getElementById("btnTodos");
    var lista = document.getElementById("lista");
    var btnLimpiar = document.getElementById('btnLimpiar');
    var archivo=document.getElementById("archivo")
    var url;
// Insertar
    var matricula = "";
    var nombre = "";
    var carrera = "";
    var genero = "";
    function leerInputs(){
        matricula = document.getElementById("matricula").value;
        nombre = document.getElementById("nombre").value;
        carrera = document.getElementById("carrera").value;
        genero = document.getElementById('genero').value;

    }
    function insertDatos(){
        leerInputs();
        var genero= document.getElementById("genero").value;
        set(ref(db,'alumnos/' + matricula),{
            nombre: nombre,
            carrera:carrera,
            genero:genero})


        .then((docRef) => {
        alert("registro exitoso");
        mostrarAlumnos();
        console.log("datos" + matricula + nombre + carrera+ genero)
        })
        .catch((error) => {
        alert("Error en el registro")
        });


        alert (" se agrego");


    };
// mostrar datos
    function mostrarAlumnos(){
        const db = getDatabase();
        const dbRef = ref(db, 'alumnos');
        onValue(dbRef, (snapshot) => {
        lista.innerHTML=""
        snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();

        lista.innerHTML = "<div class='campo'> " + lista.innerHTML +"Matricula: "+ childKey +
        " Nombre:  "+childData.nombre +" Carrera: "+ childData.carrera +" Genero: "+childData.genero +"<br> </div>";
        console.log(childKey + ":");
        console.log(childData.nombre)
        // ...
        });
        }, {
        onlyOnce: true
        });
    }
    function actualizar(){
        leerInputs();
        update(ref(db,'alumnos/'+ matricula),{
            nombre:nombre,
            carrera:carrera,
            genero :genero
        }).then(()=>{
        alert("se realizo actualizacion");
        mostrarAlumnos();
        })
        .catch(()=>{
        alert("causo Erro " + error );
        });
    }
    function escribirInpust(){
        document.getElementById('matricula').value= matricula
        document.getElementById('nombre').value= nombre;
        document.getElementById('carrera').value= carrera;
        document.getElementById('genero').value= genero;
    }
    function borrar(){
        leerInputs();
        remove(ref(db,'alumnos/'+ matricula)).then(()=>{
        alert("se borro");
        mostrarAlumnos();
        })
        .catch(()=>{
        alert("causo Erro " + error );
        });


    }
    function mostrarDatos(){
        leerInputs();
        console.log("mostrar datos ");
        const dbref = ref(db);
        get(child(dbref,'alumnos/'+ matricula)).then((snapshot)=>{
        if(snapshot.exists()){
            nombre = snapshot.val().nombre;
            carrera = snapshot.val().carrera;
            genero = snapshot.val().genero;
            console.log(genero);
            escribirInpust();
        }
        else {

            alert("No existe");
        }
        }).catch((error)=>{
            alert("error buscar" + error);
        });
    }
    function limpiar(){
        lista.innerHTML="";
        matricula="";
        nombre="";
        carrera="";
        genero=1;
        escribirInpust();
    }
    btnInsertar.addEventListener('click',insertDatos);
    btnBuscar.addEventListener('click',mostrarDatos);
    btnActualizar.addEventListener('click',actualizar);
    btnBorrar.addEventListener('click',borrar);
    btnTodos.addEventListener('click', mostrarAlumnos);
    btnLimpiar.addEventListener('click', limpiar);
    archivo.addEventListener('change',cargarImagen);

   async function cargarImagen(){
        const file=event.target.files[0];
        const name=event.target.files[0].name;

        const storage = getStorage();
        const storageRef = refS(storage, 'imagenes/'+ name);

// 'file' comes from the Blob or File API
       await uploadBytes(storageRef, file).then((snapshot) => {
        url=descargarImagen(name);
        alert('Se cargo el archivo');
        });

        // eventos


    }
    async function descargarImagen(name) {
        // Create a reference to the file we want to download
const storage = getStorage();
const starsRef = refS(storage, name);

// Get the download URL
getDownloadURL(starsRef)
  .then((url) => {
    // Insert url into an <img> tag to "download"
    document.getElementById("url").value=url;
  })
  .catch((error) => {
    // A full list of error codes is available at
    // https://firebase.google.com/docs/storage/web/handle-errors
    switch (error.code) {
      case 'storage/object-not-found':
        alert("1")
        break;
      case 'storage/unauthorized':
        alert("2")
        break;
      case 'storage/canceled':
        alert("3")
        break;

      // ...

      case 'storage/unknown':
        alert("4")
        break;
    }
  });
    }